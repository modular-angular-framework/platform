# Modular Angular Framework

## Platform

Proof of concept framework for dynamic angular UI from remote modules.

## Setup

Clone all packages and `yarn` them then start the build process with `yarn start` (concurrently) to start the independent bundle builds. `cd core` and start either the dev server `yarn start` or make a production build `yarn build` and serve `dist` with `http-server`

## Build Overview

`yarn start` runs concurrently on several commands to build the bundles using ng-packagr, watch the dist folder then copy the UMD files into `public` so that an `http-server` can serve them to the `core` Angular app.

## Credits

- David Lynam's Rebelcon 2018 talk [Our Journey To Modular UI Development](http://rebelcon.io/talks/david-lynam-journey-to-modular-ui/#talk)
- [Here is what you need to know about dynamic components in Angular](https://blog.angularindepth.com/here-is-what-you-need-to-know-about-dynamic-components-in-angular-ac1e96167f9e)
